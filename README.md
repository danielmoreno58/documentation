# Documentation

Languages: [Spanish](https://github.com/danielmoreno58/documentation/tree/master/README.es.md), [English](https://github.com/danielmoreno58/documentation/tree/master/README.md)

Here I add all the documentation that I learned from different languages, mainly in Spanish and in English.

Version control:

* [Git](https://github.com/danielmoreno58/documentation/tree/master/Git)

Web development:

* [CSS](https://github.com/danielmoreno58/documentation/tree/master/CSS)

CMS:

* [Shopify](https://github.com/danielmoreno58/documentation/tree/master/Shopify)

Software development:

* [Testing](https://github.com/danielmoreno58/documentation/tree/master/Testing)

Games:

* [Unity](https://github.com/danielmoreno58/documentation/tree/master/Unity)