# Documentación

Lenguajes: [Español](https://github.com/danielmoreno58/documentation/tree/master/README.es.md), [Inglés](https://github.com/danielmoreno58/documentation/tree/master/README.md)

Aquí he agregado toda la documentación que he aprendido de diferentes lenguajes de programación, principalmente escribo esta información en Español e Inglés.

Control de versiones:

* [Git](https://github.com/danielmoreno58/documentation/tree/master/Git)

Desarrollo web:

* [CSS](https://github.com/danielmoreno58/documentation/tree/master/CSS)

Sistema de gestión de contenidos:

* [Shopify](https://github.com/danielmoreno58/documentation/tree/master/Shopify)

Desarrollo de software:

* [Testing](https://github.com/danielmoreno58/documentation/tree/master/Testing)

Juegos:

* [Unity](https://github.com/danielmoreno58/documentation/tree/master/Unity)